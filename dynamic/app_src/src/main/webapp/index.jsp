<%@ page language="java"%>
<%@ page import="java.lang.*"%>
<%@ page import="java.util.*"%>
<%
	final String color = request.getParameter("color");
	final String staticContentURL = System.getenv("WEB_CSS_URL");

%>
<html lang="en-US">
 <head>
  <meta charset="UTF-8">
  <title>Test Page</title>
<%
	if (null != color) {
	%><link href="<%=staticContentURL%>/css/<%=color%>.css" rel="styleSheet" type="text/css"/><%
	}
%>
 </head>
  <body>
   <p>Select color:
<form> 
 <select name="color" onchange='this.form.submit()'>
  <option value="none">None</option>
  <option value="red">Red</option>
  <option value="green">Green</option>
  <option value="blue">Blue</option>
  <option value="gray">Grey/Gray</option>
 </select> 
</form>
   </p>
   <p>Referer:<%= request.getHeader("referer") %></p>
   <h1>Session Cookies</h1><% 
Cookie[] cookies = request.getCookies();
if (cookies != null) {
    for (Cookie cookie : cookies) {
      out.println (" " + cookie.getName ()+"="+cookie.getValue ()+";");        
    }
}
%></p>
  <h1>Environment</h1>
<%
	Map<String, String> env = System.getenv();
    for (String envName : env.keySet()) {
		%><p><b><%=envName%></b>=<%=env.get(envName)%>
</p><%
	}
%>  
  <h1>Properties</h1>
<%
	Properties systemProperties = System.getProperties();
	Enumeration enuProp = systemProperties.propertyNames();
	while (enuProp.hasMoreElements()) {
			String propertyName = (String) enuProp.nextElement();
			String propertyValue = systemProperties.getProperty(propertyName);
			%><p><b><%=propertyName%></b>=<%=propertyValue%>
			</p><%
	}
%>  
 </body>
</html>

